import 'package:flutter/material.dart';

enum MyColor {
  primary(color: Color(0xFF6750A4)),
  background(color: Color(0xFFF5F5F5));

  const MyColor({required this.color});
  final Color color;
}

final myTheme = ThemeData(
  useMaterial3: true,
);
