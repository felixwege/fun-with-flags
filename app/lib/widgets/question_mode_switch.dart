import 'package:app/state/question_state.dart';
import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

import '../model/question_mode_model.dart';

class QuestionModeSwitch extends ConsumerWidget {
  const QuestionModeSwitch({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) =>
      SegmentedButton<QuestionMode>(
        segments: QuestionMode.values
            .map((it) => ButtonSegment<QuestionMode>(
                  value: it,
                  label: Text(it.name),
                ))
            .toList(),
        selected: {ref.watch(questionModeProvider)},
        onSelectionChanged: (selected) {
          // set the selected mode as the current mode
          ref.read(questionModeProvider.notifier).state = selected.first;

          // Generate a new question when the mode is changed
          final question = ref.read(questionProvider.notifier).state;
          ref.read(questionProvider.notifier).state =
              generateQuestion(previousQuestion: question);

          // Reset the selected option
          ref.read(selectedOptionProvider.notifier).state = null;
        },
      );
}
