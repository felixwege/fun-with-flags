import 'package:flutter/material.dart';

import '../model/flag_model.dart';

class FlagImage extends StatelessWidget {
  const FlagImage({super.key, required this.flag});

  final Flag flag;

  @override
  Widget build(BuildContext context) => Image.asset(
        flag.imagePath,
        fit: BoxFit.contain,
      );
}
