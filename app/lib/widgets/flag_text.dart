import 'package:flutter/material.dart';

import '../model/flag_model.dart';

class FlagText extends StatelessWidget {
  const FlagText({
    super.key,
    required this.flag,
    required this.fontSize,
  });

  final Flag flag;
  final double fontSize;

  @override
  Widget build(BuildContext context) {
    return Text(
      flag.name,
      style: TextStyle(fontSize: fontSize),
    );
  }
}
