import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

import '../model/flag_model.dart';
import '../model/question_mode_model.dart';
import '../state/question_state.dart';
import 'flag_widget.dart';

class OptionsRadioGroup extends ConsumerWidget {
  const OptionsRadioGroup({super.key, required this.options});

  final List<Flag> options;

  @override
  Widget build(BuildContext context, WidgetRef ref) => Wrap(
        direction: Axis.horizontal,
        spacing: 32,
        runSpacing: 32,
        children: options
            .map((Flag option) => Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Container(
                      padding: const EdgeInsets.symmetric(horizontal: 4),
                      child: Radio<Flag>(
                        value: option,
                        groupValue: ref.watch(selectedOptionProvider),
                        onChanged: (Flag? value) {
                          ref.read(selectedOptionProvider.notifier).state =
                              value;
                        },
                      ),
                    ),
                    GestureDetector(
                      onTap: () {
                        ref.read(selectedOptionProvider.notifier).state =
                            option;
                      },
                      behavior: HitTestBehavior.opaque,
                      child: FlagWidget(
                        flag: option,
                        size: 64,
                        displayAsImage: ref.watch(questionModeProvider) ==
                            QuestionMode.showSymbol,
                        alignment: Alignment.center,
                      ),
                    ),
                  ],
                ))
            .toList(),
      );
}
