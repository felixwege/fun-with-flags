import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

import '../state/question_state.dart';

class SubmitAnswerButton extends ConsumerWidget {
  const SubmitAnswerButton({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) => ElevatedButton(
        onPressed: () {
          final question = ref.watch(questionProvider);
          final selectedOption = ref.watch(selectedOptionProvider);

          if (selectedOption == null) {
            ScaffoldMessenger.of(context).removeCurrentSnackBar();
            ScaffoldMessenger.of(context).showSnackBar(
              const SnackBar(
                duration: Duration(seconds: 2),
                behavior: SnackBarBehavior.floating,
                content: Text('Please select an option!'),
              ),
            );
            return;
          }

          if (selectedOption == question.correctOption) {
            ScaffoldMessenger.of(context).removeCurrentSnackBar();
            ScaffoldMessenger.of(context).showSnackBar(
              const SnackBar(
                duration: Duration(seconds: 2),
                behavior: SnackBarBehavior.floating,
                content: Text('Correct!'),
              ),
            );
            ref.read(questionProvider.notifier).state =
                generateQuestion(previousQuestion: question);
            ref.read(selectedOptionProvider.notifier).state = null;
            return;
          }

          ScaffoldMessenger.of(context).removeCurrentSnackBar();
          ScaffoldMessenger.of(context).showSnackBar(
            const SnackBar(
              duration: Duration(seconds: 2),
              behavior: SnackBarBehavior.floating,
              content: Text('Incorrect!'),
            ),
          );
        },
        child: const Text('Submit Answer'),
      );
}
