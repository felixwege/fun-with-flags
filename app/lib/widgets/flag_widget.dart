import 'package:flutter/widgets.dart';

import '../model/flag_model.dart';
import 'flag_image.dart';
import 'flag_text.dart';

class FlagWidget extends StatelessWidget {
  const FlagWidget({
    super.key,
    required this.flag,
    required this.size,
    required this.displayAsImage,
    this.alignment = Alignment.center,
  });

  final Flag flag;
  final double size;
  final bool displayAsImage;
  final Alignment alignment;

  @override
  Widget build(BuildContext context) => SizedBox(
        width: size,
        height: size,
        child: Align(
          alignment: alignment,
          child: displayAsImage
              ? FlagImage(flag: flag)
              : FlagText(flag: flag, fontSize: size * 0.4),
        ),
      );
}
