import 'dart:math';
import 'package:collection/collection.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

import '../model/flag_model.dart';
import '../model/question_mode_model.dart';
import '../model/question_model.dart';

Question<Flag> generateQuestion({Question<Flag>? previousQuestion}) {
  // Choose a subset of flags based on the category of a random flag
  final randomFlag = allFlags[Random().nextInt(allFlags.length)];
  List<Flag> flagsSubset = allFlags;
  switch (randomFlag.category) {
    case FlagCategory.number:
      {
        flagsSubset = allNumberFlags;
      }
    case FlagCategory.letter:
      {
        flagsSubset = allLetterFlags;
      }
  }

  const numberOfOptions = 4;
  List<Flag> options = flagsSubset.sample(numberOfOptions);
  Flag correctFlag = options[Random().nextInt(options.length)];
  if (correctFlag == previousQuestion?.correctOption) {
    // Try again to avoid repeating the same question
    return generateQuestion(previousQuestion: previousQuestion);
  }
  return Question<Flag>(correctOption: correctFlag, options: options);
}

final questionModeProvider =
    StateProvider<QuestionMode>((ref) => QuestionMode.showImage);

final questionProvider =
    StateProvider<Question<Flag>>((ref) => generateQuestion());

final selectedOptionProvider = StateProvider<Flag?>((ref) => null);
