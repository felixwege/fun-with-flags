import 'package:hooks_riverpod/hooks_riverpod.dart';

final navigationDrawerSelectionProvider = StateProvider<int>((ref) => 0);
