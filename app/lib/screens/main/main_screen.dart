import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../state/navigation_drawer_selection_state.dart';
import '../../theme.dart';
import 'list_of_flags_widget.dart';
import 'practise_flags_widget.dart';

class MainScreen extends ConsumerWidget {
  const MainScreen({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return Scaffold(
      backgroundColor: MyColor.background.color,
      appBar: AppBar(
        backgroundColor: MyColor.background.color,
        systemOverlayStyle: SystemUiOverlayStyle(
          statusBarColor: MyColor.primary.color,
          statusBarIconBrightness: Brightness.light,
        ),
        leading: Builder(
          builder: (context) {
            return IconButton(
              icon: const Icon(Icons.menu),
              onPressed: () {
                Scaffold.of(context).openDrawer();
              },
            );
          },
        ),
      ),
      drawer: NavigationDrawer(
        selectedIndex: ref.watch(navigationDrawerSelectionProvider),
        onDestinationSelected: (index) async {
          if (index == 2) {
            // Close the drawer without resetting the selected index
            Navigator.pop(context);

            // Open the repository in the current tab
            final Uri url = Uri.parse(
              "https://gitlab.com/felixwege/fun-with-flags",
            );
            await launchUrl(url, webOnlyWindowName: "_self");
            return;
          }

          // Set the selected index and close the drawer
          ref.read(navigationDrawerSelectionProvider.notifier).state = index;
          Navigator.pop(context);
        },
        children: [
          const SizedBox(height: 32),
          NavigationDrawerDestination(
            icon: const Icon(Icons.fitness_center_outlined),
            label: const Text("Practise"),
            selectedIcon: Icon(
              Icons.fitness_center,
              color: MyColor.primary.color,
            ),
          ),
          NavigationDrawerDestination(
            icon: const Icon(Icons.flag_outlined),
            label: const Text("List of Flags"),
            selectedIcon: Icon(
              Icons.flag,
              color: MyColor.primary.color,
            ),
          ),
          NavigationDrawerDestination(
            icon: const Icon(Icons.code_outlined),
            label: const Text("Repository"),
            selectedIcon: Icon(
              Icons.code,
              color: MyColor.primary.color,
            ),
          ),
        ],
      ),
      body: MainScreenBody(
        index: ref.watch(navigationDrawerSelectionProvider),
      ),
    );
  }
}

class MainScreenBody extends StatelessWidget {
  const MainScreenBody({super.key, required this.index});

  final int index;

  @override
  Widget build(BuildContext context) {
    switch (index) {
      case 0:
        return const PractiseFlagsWidget();
      case 1:
        return const ListOfFlagsWidget();
      case 2:
        return const PractiseFlagsWidget();
      default:
        return const PractiseFlagsWidget();
    }
  }
}
