import 'dart:math';

import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

import '../../model/flag_model.dart';
import '../../model/question_mode_model.dart';
import '../../model/question_model.dart';
import '../../state/question_state.dart';
import '../../widgets/flag_widget.dart';
import '../../widgets/options_radio_group.dart';
import '../../widgets/question_mode_switch.dart';
import '../../widgets/submit_answer_button.dart';

class PractiseFlagsWidget extends ConsumerWidget {
  const PractiseFlagsWidget({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    Question<Flag> q = ref.watch(questionProvider);
    return SingleChildScrollView(
      child: Container(
        padding: EdgeInsets.fromLTRB(
          32,
          0.1 * MediaQuery.of(context).size.height,
          32,
          32,
        ),
        alignment: Alignment.center,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            const QuestionModeSwitch(),
            const SizedBox(height: 32),
            FlagWidget(
              flag: q.correctOption,
              size: min(0.7 * MediaQuery.of(context).size.width, 400),
              displayAsImage:
                  ref.watch(questionModeProvider) == QuestionMode.showImage,
            ),
            const SizedBox(height: 32),
            OptionsRadioGroup(
              options: q.options as List<Flag>, // TODO why is this necessary?
            ),
            const SizedBox(height: 32),
            const SubmitAnswerButton(),
          ],
        ),
      ),
    );
  }
}
