import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

import '../../model/flag_model.dart';
import '../../widgets/flag_text.dart';
import '../../widgets/flag_widget.dart';

class ListOfFlagsWidget extends ConsumerWidget {
  const ListOfFlagsWidget({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return GridView.builder(
      padding: const EdgeInsets.all(32),
      gridDelegate: const SliverGridDelegateWithMaxCrossAxisExtent(
        maxCrossAxisExtent: 128,
        mainAxisSpacing: 32,
        crossAxisSpacing: 32,
      ),
      itemCount: allFlags.length,
      itemBuilder: (context, index) {
        return Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            FlagWidget(
              flag: allFlags[index],
              size: 64,
              displayAsImage: true,
            ),
            const SizedBox(width: 8),
            FlagText(
              flag: allFlags[index],
              fontSize: 32,
            )
          ],
        );
      },
    );
  }
}
