import 'package:freezed_annotation/freezed_annotation.dart';

part 'flag_model.freezed.dart';

enum FlagCategory {
  number,
  letter,
}

@freezed
class Flag with _$Flag {
  const factory Flag({
    required String name,
    required String imagePath,
    required FlagCategory category,
  }) = _Flag;
}

const allNumberFlagNames = [
  "0",
  "1",
  "2",
  "3",
  "4",
  "5",
  "6",
  "7",
  "8",
  "9",
];

List<Flag> allNumberFlags = allNumberFlagNames
    .map((name) => Flag(
          name: name,
          imagePath: 'assets/flags/$name.png',
          category: FlagCategory.number,
        ))
    .toList();

const allLetterFlagNames = [
  "A",
  "B",
  "C",
  "D",
  "E",
  "F",
  "G",
  "H",
  "I",
  "J",
  "K",
  "L",
  "M",
  "N",
  "O",
  "P",
  "Q",
  "R",
  "S",
  "T",
  "U",
  "V",
  "W",
  "X",
  "Y",
  "Z",
];

List<Flag> allLetterFlags = allLetterFlagNames
    .map((name) => Flag(
          name: name,
          imagePath: 'assets/flags/$name.png',
          category: FlagCategory.letter,
        ))
    .toList();

const allFlagNames = [...allNumberFlagNames, ...allLetterFlagNames];
List<Flag> allFlags = [...allNumberFlags, ...allLetterFlags];
