enum QuestionMode {
  showImage(name: 'Show Image'),
  showSymbol(name: 'Show Symbol');

  const QuestionMode({required this.name});

  final String name;
}
