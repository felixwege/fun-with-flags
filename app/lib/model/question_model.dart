import 'package:freezed_annotation/freezed_annotation.dart';

part 'question_model.freezed.dart';

@freezed
class Question<T> with _$Question {
  const factory Question({
    required T correctOption,
    required List<T> options,
  }) = _Question;
}
