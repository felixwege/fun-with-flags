# app

Install dependencies
```bash
flutter pub get
```

Run build runner
```bash
dart pub run build_runner build
```

Run locally
```bash
flutter run -d chrome
```
